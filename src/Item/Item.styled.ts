import styled from 'styled-components';

export const Card = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  width: 100%;
  height: 100%;
  padding: 8px;
  border-radius: 0 12px 12px 0;
  box-shadow: 10px 0 0 0 #91c5a9;
  border: 3px solid #91c5a9;
  
  .card-body{
    text-align: center;
    line-height: 20px;
  }
  .img{
    width: 100%;
  }
  img{
    margin: 0 auto;
    display: block;
    width: 65%;
    height: 240px;
  }
  
  .info{
    display: flex;
    justify-content: space-between;
    margin-top: auto;
  }
  
  .title h4{
    color: #91c5a9;
  }
`