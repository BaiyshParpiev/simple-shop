import React from 'react';
import {Button} from '@material-ui/core';
import {Card} from './Item.styled';
import {CartItemType} from '../App';

type Props = {
    item: CartItemType,
    handleAddToCart: (clickedItem: CartItemType) => void,
}


const Item: React.FC<Props> = ({item, handleAddToCart}) => {

    return (
        <Card>
            <div className="img">
                <img src={item.image} alt={item.title}/>
            </div>
            <div className="card-body">
                <div className="title">
                    <h4>{item.title}</h4>
                </div>
                <div className="description">
                    {item.description}
                </div>
            </div>
            <div className="info">
                <h3>${item.price}</h3>
                <div>
                    <Button
                        variant="outlined"
                        color="primary"
                        onClick={() => handleAddToCart(item)}
                    >
                        Add to cart
                    </Button>
                </div>
            </div>
        </Card>
    );
};

export default Item;